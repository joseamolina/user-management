<?php
namespace App\Controller;

use App\Entity\User;

use DateTime;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserController extends Controller

{

    public $mensaje = false;

    /**
    *   @Route("/")
    */
    public function number()
    {

        $repository = $this->getDoctrine()->getRepository(User::class);

        $users = $repository->findAll();

        return $this->render('user/user.html.twig', array(
            'users' => $users,
        ));
    }

    /**
     * @Route("/darAlta")
     */
    public function darAltaUsers(Request $request) {

        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('Nombre', TextType::class)
            ->add('Apellidos', TextType::class)
            ->add('Telefono', IntegerType::class)
            ->add('Email', EmailType::class)
            ->add('save', SubmitType::class, array('label' => 'Guardar usuario'))
            ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();

            $user->setFechaAlta(new DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->render('user/success.html.twig');

        }

        return $this->render('user/createuser.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}